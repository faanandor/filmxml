package filmxml;

public class Film {
    // Létrehozzuk a változókat aminek értékeit ki szeretnénk nyerni a fájlból
    private String alkoto;
    private Integer bevetel;
    private String cim;
    private boolean isTopSzaz;
    private Integer hossz;
    
    // Létrehozzuk a gettereket és settereket
    public String getAlkoto() {
        return alkoto;
    }

    public void setAlkoto(String alkoto) {
        this.alkoto = alkoto;
    }

    public Integer getBevetel() {
        return bevetel;
    }

    public void setBevetel(Integer bevetel) {
        this.bevetel = bevetel;
    }

    public String getCim() {
        return cim;
    }

    public void setCim(String cim) {
        this.cim = cim;
    }

    public boolean isIsTopSzaz() {
        return isTopSzaz;
    }

    public void setIsTopSzaz(boolean isTopSzaz) {
        this.isTopSzaz = isTopSzaz;
    }

    public Integer getHossz() {
        return hossz;
    }

    public void setHossz(Integer hossz) {
        this.hossz = hossz;
    }
}
