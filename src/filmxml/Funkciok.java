package filmxml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Funkciok<Osztaly> {
    public void betoltes(Osztaly peldany, LinkedList<String> lista, LinkedList<Integer> listaInt) throws IOException {
        String osztalynev = peldany.getClass().getSimpleName();
        String filePath = peldany.getClass().getSimpleName() + ".xml"; // Lekérjük a fájl nevét osztály szerint
        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName(osztalynev);
            List<Film> dataList = new ArrayList<Film>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                dataList.add(getAlkotas(nodeList.item(i), lista, listaInt)); // Elmentjük az adatok node listába amit szétbontunk majd
            }
        }
        catch (SAXException | ParserConfigurationException | IOException e1) {
            e1.printStackTrace();
        }
    }
    
    // Függvény a film adattípusainak kinyeréséhez
    private static Film getAlkotas(Node node, LinkedList<String> cimList, LinkedList<Integer> profitList) { // Paraméterbe lista kell
        Film alkotas = new Film();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            String cim; int bevetel; // A kinyerendő adatokhoz változók (nekem személy szerint csak ezek kellettek, de lehet többet is)
            Element element = (Element) node;
            alkotas.getCim(); // Az adatokat elmentjük listába, így könnyen tárolhatjuk és használhatjuk őket
            cim = getTagValue("cim", element); cimList.add(cim);
            alkotas.getBevetel(); // Itt szintén
            bevetel = Integer.parseInt(getTagValue("bevetel", element)); profitList.add(bevetel);
        }
        return alkotas;
    }
    
    // Függvény a film adatainak kinyeréséhez
    private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }
}
