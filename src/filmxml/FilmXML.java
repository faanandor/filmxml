package filmxml;

import filmxml.Funkciok;
import java.io.IOException;
import java.util.LinkedList;

public class FilmXML {
    public static void main(String[] args) {
        LinkedList<String> cimLista = new LinkedList<String>(); // A listákba könnyen eltárolhatjuk a szükséges adatokat
        LinkedList<Integer> bevetelLista = new LinkedList<Integer>();
        Film a = new Film();
        try {
            Funkciok<Film> f = new Funkciok<Film>(); // Betöltjük a fájlt és mentünk a listákba
            f.betoltes(a, cimLista, bevetelLista);
        }
        catch (IOException err) {
            System.out.println("Hiba: " + err.getMessage());
        }
        // Kiírjuk a lista adatait, itt már akár kereshetünk is, hogy az adott film szerepel-e a fájlban vagy sem
        System.out.println("Címek: " + cimLista);
        System.out.println("Bevételek: " + bevetelLista);
    }
}
